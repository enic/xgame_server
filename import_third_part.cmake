
# 设置third_part目录
SET(XENV_THIRD_PART_PATH ${CMAKE_SOURCE_DIR}/xthird_part/prebuild/)


#get_target_property(vvv pypythoncore IMPORTED_IMPLIB_DEBUG)
#MESSAGE("------------::" ${vvv})

#ImportedTargets：导入已经存在的依赖
#  IMPORTED_IMPLIB_<CONFIG>: 在DLL平台上表示DLL的lib路径
#  IMPORTED_LOCATION_<CONFIG>: 目标文件完整文件路径。静态库表示库文件或者模块。动态库在非DLL平台上表示动态库路径,支持DLL的平台上表示目标dll文件


####################################################################################################################
# 设置pypythoncore目录
SET(XENV_PYPYTHONCORE_INCLUDE ${XENV_THIRD_PART_PATH}/python/win/include/)
SET(XENV_PYPYTHONCORE_LIB ${XENV_THIRD_PART_PATH}/python/win/lib/)

# 设置pypythoncore为外部库
ADD_LIBRARY(pypythoncore SHARED IMPORTED)
SET_TARGET_PROPERTIES(pypythoncore PROPERTIES IMPORTED_IMPLIB ${XENV_PYPYTHONCORE_LIB}python310.lib)
SET_TARGET_PROPERTIES(pypythoncore PROPERTIES IMPORTED_LOCATION ${XENV_PYPYTHONCORE_LIB}python310.dll)
SET_TARGET_PROPERTIES(pypythoncore PROPERTIES IMPORTED_IMPLIB_DEBUG ${XENV_PYPYTHONCORE_LIB}python310.lib)
SET_TARGET_PROPERTIES(pypythoncore PROPERTIES IMPORTED_LOCATION_DEBUG ${XENV_PYPYTHONCORE_LIB}python310.dll)
####################################################################################################################
####################################################################################################################
# 设置gperf目录
SET(XENV_GPERF_INCLUDE ${XENV_THIRD_PART_PATH}/gperftools/win/include/)
SET(XENV_GPERF_LIB ${XENV_THIRD_PART_PATH}/gperftools/win/lib/)
ADD_LIBRARY(gperf_tcmalloc STATIC IMPORTED)
SET_TARGET_PROPERTIES(gperf_tcmalloc PROPERTIES IMPORTED_LOCATION ${XENV_GPERF_LIB}/tcmalloc_minimal_static.lib)
SET_TARGET_PROPERTIES(gperf_tcmalloc PROPERTIES IMPORTED_LOCATION_DEBUG ${XENV_GPERF_LIB}/tcmalloc_minimal_static_d.lib)
ADD_LIBRARY(gperf_logging STATIC IMPORTED)
SET_TARGET_PROPERTIES(gperf_logging PROPERTIES IMPORTED_LOCATION ${XENV_GPERF_LIB}/gperf_logging.lib)
SET_TARGET_PROPERTIES(gperf_logging PROPERTIES IMPORTED_LOCATION_DEBUG ${XENV_GPERF_LIB}/gperf_logging_d.lib)
ADD_LIBRARY(gperf_spinlock STATIC IMPORTED)
SET_TARGET_PROPERTIES(gperf_spinlock PROPERTIES IMPORTED_LOCATION ${XENV_GPERF_LIB}/gperf_spinlock.lib)
SET_TARGET_PROPERTIES(gperf_spinlock PROPERTIES IMPORTED_LOCATION_DEBUG ${XENV_GPERF_LIB}/gperf_spinlock_d.lib)
ADD_LIBRARY(gperf_sysinfo STATIC IMPORTED)
SET_TARGET_PROPERTIES(gperf_sysinfo PROPERTIES IMPORTED_LOCATION ${XENV_GPERF_LIB}/gperf_sysinfo.lib)
SET_TARGET_PROPERTIES(gperf_sysinfo PROPERTIES IMPORTED_LOCATION_DEBUG ${XENV_GPERF_LIB}/gperf_sysinfo_d.lib)
####################################################################################################################
####################################################################################################################
# 设置libuv目录
SET(XENV_LIBUV_INCLUDE ${XENV_THIRD_PART_PATH}/libuv/win/include/)
SET(XENV_LIBUV_LIB ${XENV_THIRD_PART_PATH}/libuv/win/lib/)
ADD_LIBRARY(libuv_a STATIC IMPORTED)
SET_TARGET_PROPERTIES(libuv_a PROPERTIES IMPORTED_LOCATION ${XENV_LIBUV_LIB}/uv_a.lib)
SET_TARGET_PROPERTIES(libuv_a PROPERTIES IMPORTED_LOCATION_DEBUG ${XENV_LIBUV_LIB}/uv_a_d.lib)
