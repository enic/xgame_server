

SET(src 
    test_entry.cc
    socket.hh
    socket.cc
)
ADD_EXECUTABLE(xnet ${src})
TARGET_LINK_LIBRARIES(xnet libuv_a ws2_32.lib IPHLPAPI.lib Userenv.lib)
