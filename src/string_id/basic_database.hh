/**
 * @file basic_database.hh
 * @author enic (errorcpp@qq.com)
 * @brief 
 * @version 0.1
 * @date 2022-02-21
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef __HEAD_GUID_b77c2923c6574abe8898903da818e5dc
#define __HEAD_GUID_b77c2923c6574abe8898903da818e5dc

#include <string>
#include "hash_func.hh"

namespace xstr {

class BasicDatabase
{
protected:
    BasicDatabase() = default;
public:
    BasicDatabase(const BasicDatabase&) = delete;
    BasicDatabase(BasicDatabase&&) = delete;

public:
    virtual ~BasicDatabase() = default;

    enum class InsertStatus : int32_t
    {
        kCollisison,  // 冲突(hash冲突)
        kNewString,   // 插入的新的字符串
        kOldString,   // 插入的是已经存在的字符串
    };

    /**
     * @brief 将hash-string pari插入到DB中
     * 
     * @param hash str对应的哈希值
     * @param str 目标字符串
     * @return InsertStatus 插入状态
     */
    virtual InsertStatus insert(hash64_result_t hash, const char* str) = 0;

    /**
     * @brief 使用给定的哈希值查询对应的源字符串
     * 
     * @param hash 
     * @return const char* 
     */
    virtual const char* lookup(hash64_result_t hash) const = 0;
};

};  //~  end of namespace xstr
#endif