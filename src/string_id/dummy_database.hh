/**
 * @file dummy_database.hh
 * @author enic (errorcpp@qq.com)
 * @brief 
 * @version 0.1
 * @date 2022-02-21
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef __HEAD_GUID_080e1997985a49a19c09b77e24f57bff
#define __HEAD_GUID_080e1997985a49a19c09b77e24f57bff

#include "basic_database.hh"

namespace xstr {

class DummyDatabase : public BasicDatabase
{
public:
    virtual InsertStatus insert(hash64_result_t, const char*, std::size_t)
    {
        return InsertStatus::kNewString;
    }

    const char* lookup(hash64_result_t) const
    {
        return "!!! string_id database disabled";
    }
};

};  //~  end of namespace xstr
#endif