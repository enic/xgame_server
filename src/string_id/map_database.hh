/**
 * @file map_database.hh
 * @author enic (errorcpp@qq.com)
 * @brief 
 * @version 0.1
 * @date 2022-02-21
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef __HEAD_GUID_a46db13b64e24611bf777b631e609a93
#define __HEAD_GUID_a46db13b64e24611bf777b631e609a93

#include <vector>
#include "basic_database.hh"

namespace xstr {

class MapDatabase : public BasicDatabase
{
private:
    class NodeList;

public:
    MapDatabase(std::size_t cnt = 1024, double max_load_factor = 1.0);
    virtual ~MapDatabase();

    virtual InsertStatus insert(hash64_result_t hash, const char* str, std::size_t cnt);

    virtual const char* lookup(hash64_result_t hash) const;

protected:
    void rehash();

private:
    std::vector<NodeList>  buckets_;
    std::size_t  node_items_;
    std::size_t  node_buckets_;
    double  max_load_factor_;
    std::size_t  next_resize_;
};  //~

};  //~  end of namespace xstr

#endif