/**
 * @file hash_func.hh
 * @author enic (errorcpp@qq.com)
 * @brief 
 * @version 0.1
 * @date 2022-02-21
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef __HEAD_GUID_a961a879550446ec952b6b485c47e1d2
#define __HEAD_GUID_a961a879550446ec952b6b485c47e1d2

#include <cstdint>

namespace xstr {

/**
 * 哈希库参考地址：https://github.com/ron4fun/HashLibPlus/tree/main/Hash32
 * FNV算法：
 *   特点和用途：FNV能快速hash大量数据并保持较小的冲突率，它的高度分散使它适用于hash一些非常相近的字符串，比如URL，hostname，文件名，text，IP地址等
 *   适用范围：字符串比较短的哈希场景
 * 
 * TIMES33(DJB)算法：
 *   使用场景：字符串哈希，计算速度快，分布均匀
 **/

typedef uint32_t hash32_result_t;
typedef uint64_t hash64_result_t;

const hash64_result_t fnv_basis = 14695981039346656037ull;
const hash64_result_t fnv_prime = 1099511628211ull;

// FNV-1a 64 bit hash
hash64_result_t FNV1A64_hash(const char *str, const hash64_result_t hash = fnv_basis)
{
    return *str ? FNV1A64_hash(str + 1, (hash ^ *str) * fnv_prime) : hash;
}

};  //~  end of namespace xstr
#endif