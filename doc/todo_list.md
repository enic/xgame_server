1.一种新的日志记录思路
https://gitee.com/mirrors/binary-log

2.日志优化
https://www.cnblogs.com/kingstarer/p/10359022.html
https://my.oschina.net/u/4360182/blog/3243553
https://xin-tan.com/2019-09-04-log-module/
http://armsword.com/2016/09/10/a-high-performace-and-thread-safe-cpp-log-lib/
https://blog.nowcoder.net/n/382e206188f547f19abf24281ab7846f
https://zhuanlan.zhihu.com/p/367100699
https://juejin.cn/post/6949067257142067208

3.telegram
https://github.com/tdlib/td
https://github.com/tdlib/telegram-bot-api
https://github.com/telegramdesktop/tdesktop

4.跨语言绑定类dom\com机制
https://www.oschina.net/p/scriptx
https://zhuanlan.zhihu.com/p/27160461?ivk_sa=1024320u
https://github.com/luzhlon/xobj

5.websocket
https://github.com/luzhlon/websocket-cpp

6.使用调试引擎
https://github.com/luzhlon/wdbg

7.垃圾回收
https://www.cnblogs.com/mengfanrong/p/3741033.html   ref+mark
https://gist.github.com/nbdd0121/b37457d9c27ec82c592a  mark-swap例子
https://www.cnblogs.com/woshiweige/p/4537451.html
https://github.com/munificent/mark-sweep

8.lua分析 考虑从1.1开始分析
https://blog.csdn.net/jinxinliu1/article/details/80484675  vm指令集
https://blog.csdn.net/jinxinliu1/article/details/80484675
https://zhuanlan.zhihu.com/p/97722378